<?php

/**
 * @file
 * Contains \Drupal\temporary_page_access\Form\ContributeForm.
 */

namespace Drupal\temporary_page_access\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Provides temporary page access settings configuration form.
 */
class TemporaryPageAccessSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['temporary_page_access.settings'];
  }

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'temporary_page_access_settings';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('temporary_page_access.settings');

    $form['temporary_page_access_fieldset'] = array(
      '#type' => 'details',
      '#title' => $this->t('Temporary Page Access Settings'),
      '#description' => $this->t('Configure message related settings.'),
      '#open' => TRUE,
     );
    
    $form['temporary_page_access_fieldset']['message_headline'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message Headline'),
      '#default_value' => $config->get('message.headline'),
      '#size' => 40,
      '#description' => $this->t('This text will be message box headline.'),
    ];

    $form['temporary_page_access_fieldset']['message_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to display in the message box'),
      '#default_value' => $config->get('message.body'),
      '#size' => 40,
      '#description' => $this->t('This message must be plain text.'),
    ]; 
    
    $form['temporary_page_access_fieldset']['close_button_text'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Close Button Text'),
      '#default_value' => $config->get('message.close_button_text'),
      '#description' => $this->t('Enter the text for the close button.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('temporary_page_access.settings');
    $config->set('message.headline', $values['message_headline']) 
      ->set('message.body', $values['message_body'])
      ->set('message.close_button_text', $values['close_button_text']) 
      ->save();
    parent::submitForm($form, $form_state); 
  }

}
