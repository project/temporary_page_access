<?php

/**
 * @file
 * Contains \Drupal\temporary_page_access\Form\ContributeForm.
 */

namespace Drupal\temporary_page_access\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TemporaryPageAccessForm extends FormBase {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
   /**
   * The alias manager that caches alias lookups based on the request.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;
  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;
 
  public function __construct(ConfigFactoryInterface $configFactory, AliasManagerInterface $alias_manager, PathValidatorInterface $path_validator) {
    $this->configFactory = $configFactory;
    $this->aliasManager = $alias_manager;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'), $container->get('path_alias.manager'),$container->get('path.validator') 
    );
  }
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'temporary_page_access_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $paths = $this->getCurrentPageAccess();
 
        $form['temporary_page_access_table'] = [
            '#type' => 'table',
            '#header' => ['Enabled', 'Relative path', 'Expiration DateTime'],
            '#title' => $this->t('Temporary Page Access'),
            '#attributes' => ['id' => 'temporary-page-access-wrapper'],
        ];

        $storage = $form_state->getStorage();
        if (empty($storage['number-of-rows'])) {
            $storage['number-of-rows'] = count($paths) + 1;
            $form_state->setStorage($storage);
        }

        for ($i = 0; $i < $storage['number-of-rows']; $i++) {
            $form['temporary_page_access_table'][$i] = $this->buildFormPageRow($paths, $i);
        }

        $form['actions'] = [
            '#type' => 'actions',
        ];

        $form['actions']['add-row'] = [
            '#type' => 'submit',
            '#value' => $this->t('Add extra row'),
            '#submit' => ['::addExtraRow'],
            '#ajax' => [
                'callback' => '::addExtraRowAjaxCallback',
                'wrapper' => 'temporary-page-access-wrapper',
            ],
        ];

        $form['actions']['remove'] = [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
        ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save configuration'),
        ];
        return $form;
    }

    /**
     * Simplify buildForm function.
     *
     * @param array $paths
     *   Data for all paths.
     * @param int $row_no
     *   Row number.
     *
     * @return array
     *   One row for form table.
     */
    private function buildFormPageRow($paths, $row_no) {

        if (!empty($paths[$row_no])) {
            $paths = $paths[$row_no];
        } else {
            $paths = [
                'enabled' => TRUE,
                'relative_path' => '',
                'expiration_datetime' => '',
            ];
        }
        $row = [];
        $row['enabled'] = [
            '#type' => 'checkbox',
            '#maxlength' => 255,
            '#default_value' => $paths['enabled'],
            '#attributes' => array(
                'title' => t('Flag whether this override should be active.'),
            ),
        ];

        $row['relative_path'] = [
            '#type' => 'textfield',
            '#default_value' => $paths['relative_path'],
            '#description' => $this->t('Enter relative drupal path. For example, "/node/30"'),
        ];
            
        $row['expiration_datetime'] = [
            '#type' => 'datetime',
            '#default_value' => !empty($paths['expiration_datetime']) ? DrupalDateTime::createFromTimestamp($paths['expiration_datetime']) : '',
            '#size' => 20,
            '#date_date_format' => 'Y-m-d',
            '#date_time_format' => 'H:i:s', 
            '#element_validate' => [],
        ];

        return $row;
    }

    public function getCurrentPageAccess() {

        $paths_enabled = $this->configFactory->getEditable('temporary_page_access.enabled')
                         ->get('contexts');
        $paths_disabled = $this->configFactory->getEditable('temporary_page_access.disabled')
                         ->get('contexts');
        $data = [
            FALSE => $paths_disabled ? $paths_disabled : [],
            TRUE => $paths_enabled ? $paths_enabled : [],
        ];

        $paths = [];
        foreach ($data as $enabled => $rows) {
            foreach ($rows as $row) {
                foreach ($row['items'] as $key => $value) {
                    $paths[] = [
                        'enabled' => $enabled,
                        'relative_path' => mb_strtolower($value['relative_path']),
                        'expiration_datetime' => $value['expiration_datetime'],
                    ];
                }
            }
        }

        return $paths;
    }

    /**
     * Submit handler to add extra row.
     *
     * @param array $form
     *   Drupal form array.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Drupal form state object.
     */
    public function addExtraRow(array $form, FormStateInterface $form_state) {
        $storage = $form_state->getStorage();
        $storage['number-of-rows'] ++;
        $form_state->setStorage($storage);
        $form_state->setRebuild(TRUE);
    }

    /**
     * Callback for Ajax functionality.
     *
     * @param array $form
     *   Drupal form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   Drupal form state object.
     *
     * @return mixed
     *   Form element for ajax response, it will replace table in browser.
     */
    public function addExtraRowAjaxCallback(array $form, FormStateInterface $form_state) {
        return $form['temporary_page_access_table'];
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
      $form_data = $form_state->getValue('temporary_page_access_table');
        foreach($form_data as $i => $row) {
              if (!empty($row['relative_path'])) {
                $entered_path = rtrim(trim(mb_strtolower($row['relative_path'])), " \\/");
                if (substr($entered_path, 0, 1) != '/') {
                  $form_state->setErrorByName('relative_path', $this->t('The path needs to start with a slash.'));
                  break;
                } else {
                  $normal_path = $this->aliasManager->getPathByAlias(mb_strtolower($row['relative_path']));
                  if (!$this->pathValidator->isValid($normal_path)) {
                    $form_state->setErrorByName('relative_path',$this->t('Please enter a correct path!'));
                    break;
                  }
              }
          }
       }
   }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $paths = [TRUE => [], FALSE => []];
        $config_enabled = $this->configFactory->getEditable('temporary_page_access.enabled');
        $config_disabled = $this->configFactory->getEditable('temporary_page_access.disabled');
        $form_data = $form_state->getValue('temporary_page_access_table');

        foreach ($form_data as $i => $row) {
                $date_in_seconds = '';$time_seconds='';
            if (!empty($row['relative_path'])) {
                $date_in_seconds = strtotime($row['expiration_datetime']['date']);
                $time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $row['expiration_datetime']['time']);
                sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
                $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
                $paths[$row['enabled']]['temporarypage']['items'][] = [
                    'relative_path' => mb_strtolower($row['relative_path']),
                    'expiration_datetime' => $date_in_seconds + $time_seconds,
                ];
            }
        }

        $paths[TRUE] = array_values($paths[TRUE]);
        $paths[FALSE] = array_values($paths[FALSE]);
        $config_enabled->set('contexts', $paths[TRUE]);
        $config_enabled->save();

        switch ($form_state->getTriggeringElement()['#id']) {
            case 'edit-submit':
                $config_disabled->set('contexts', $paths[FALSE]);
                $config_disabled->save();
                $this->messenger()->addStatus(t('Your changes have been saved.'));
                break;
            case 'edit-remove':
                $config_disabled->delete();
                $this->messenger()->addStatus(t('The disabled path have been removed.'));
                break;
        }
    }

}

?>
