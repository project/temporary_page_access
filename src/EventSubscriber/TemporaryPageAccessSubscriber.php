<?php

namespace Drupal\temporary_page_access\EventSubscriber;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class TemporaryPageAccessSubscriber implements EventSubscriberInterface {

   /**
   * The temporary page access config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
   /**
   * The alias manager that caches alias lookups based on the request.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;


  /**
   * Constructs a new TemporaryPageAccessSubscriber instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */

   public function __construct(ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, CurrentPathStack $current_path ) {
    $this->configFactory = $config_factory->get('temporary_page_access.enabled');
    $this->aliasManager = $alias_manager;
    $this->currentPath = $current_path;
   }


   public function onRespond(ResponseEvent $event) {
    $response = $event->getResponse();
    if (!$response instanceof AttachmentsInterface) {
      return;
    }
    $normalPath = $this->aliasManager->getPathByAlias($this->currentPath->getPath());
    $contexts = $this->configFactory->get('contexts');
    if (!empty($contexts)) {
        foreach ($contexts as $context) {
          foreach ($context['items'] as $item) {
              if(($this->currentPath->getPath() === $item['relative_path']) || ($normalPath === $item['relative_path']) ) {
                $response->addAttachments(['drupalSettings' => ['temporary_page_access' => ['timeToExpire' => $item['expiration_datetime']]]]);
                $response->addAttachments(['library' => ['temporary_page_access/styling']]);
                break;
            }
          }
        }
     }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond', 27];
    return $events;
  }

}
