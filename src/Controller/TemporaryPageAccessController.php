<?php

/**
 * @file
 * Contains \Drupal\temporary_page_access\Controller\TemporaryPageAccessController.
 */
 
namespace Drupal\temporary_page_access\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines TemporaryPageAccessController class.
 */
class TemporaryPageAccessController extends ControllerBase {

   /**
   * The temporary page access config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

   /**
   * Constructs a TemporaryPageAccessController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static( 
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxGetMessage() {  
    $config = $this->configFactory->get('temporary_page_access.settings');
    $response = new AjaxResponse();
    $markup .= '<div class="temporary-message-wrapper">'; 
    $markup .= '<div class="temporary-message-headline">'; 
    $markup .= '<h2>'.$config->get('message.headline').'</h2>'; 
    $markup .= '</div>'; 
    $markup .= '<div class="temporary-message-body">'; 
    $markup .= '<p> '.$config->get('message.body').' </p></div>';
    $markup .= '<div class="temporary-message-footer">';
    $markup .= '<button class="temporary-btn"> '.$config->get('message.close_button_text').'</button>';
    $markup .= '</div>'; 
    $markup .= '</div>';  
    $response->addCommand(new ReplaceCommand('body', $markup)); 
    return $response;
  }

}
