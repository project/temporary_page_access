(function($, Drupal, drupalSettings) {
 Drupal.behaviors.temporarypageaccess = {
  attach: function() { 
   console.log("100000008");    

   let activeItem = Drupal.url(drupalSettings.path.currentPath);
   
   const pageExpirationTime = parseInt(drupalSettings.temporary_page_access.timeToExpire); 
   
   localStorage.setItem('pageToBeExpired'+activeItem, Math.floor(new Date(pageExpirationTime * 1000).getTime() / 1000)); 

   setInterval(function() { 
    const timestamp = Math.floor(new Date().getTime() / 1000); 
    const pageExpirationNotify = localStorage.getItem('pageToBeExpired'+activeItem);
    console.log('TTimestamp' + timestamp);
    let messageBox = false; 
    if (timestamp >= pageExpirationNotify) {
     // Session has expired.  
     if (!messageBox) {
      const ajaxObject = Drupal.ajax({  
          url: drupalSettings.path.baseUrl + 'temporary_page_access_ajax_get_message',
          submit: {js: true}
        }); 
        ajaxObject.execute();
        messageBox = true; 
     }
    } else { 
     messageBox = false;
    }
   }, 1000);

  }
 };
})(jQuery, Drupal, drupalSettings)
